package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.common.MD5Utils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {


    /**
     * 登陆
     * @param dto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto dto) {

        // 判断用户是否登陆
        if (StringUtils.isNotBlank(dto.getPhone()) && StringUtils.isNotBlank(dto.getPassword())){
            // 普通用户

            // 1.获取用户信息
            ApUser apUser = this.getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, dto.getPhone()));
            if (apUser == null){
                return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
            }

            // 2.密码比对
            String pwd = dto.getPassword();
            String salt = apUser.getSalt();
            // 生成加盐后的密文
            String md5Pwd = MD5Utils.encodeWithSalt(pwd,salt);
            if (!md5Pwd.equals(apUser.getPassword())){
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }
            // 3.返回结果
            Map<String,Object> resultMap = new HashMap<>();
            String token = AppJwtUtil.getToken(apUser.getId().longValue());
            resultMap.put("token",token);

            // 为了数据安全，将盐和密码置空
            apUser.setSalt("");
            apUser.setPassword("");

            resultMap.put("user",apUser);

            // 返回成功信息
            return ResponseResult.okResult(resultMap);
        } else {
            // 游客
            // 1.返回id为0的token
            Map<String,Object> resultMap = new HashMap<>();
            resultMap.put("token",AppJwtUtil.getToken(0L));
            ApUser tmpUser = new ApUser();
            tmpUser.setId(0);
            resultMap.put("user",tmpUser);
            // 返回成功信息
            return ResponseResult.okResult(resultMap);
        }
    }
}