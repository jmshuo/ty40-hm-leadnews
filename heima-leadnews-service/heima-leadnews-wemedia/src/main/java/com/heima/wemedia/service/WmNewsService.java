package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsListDto;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.service
 */
public interface WmNewsService extends IService<WmNews> {

    /**
     * 分页查询文章列表
     * @param dto
     * @return
     */
    PageResponseResult listForPage(WmNewsListDto dto);
}
