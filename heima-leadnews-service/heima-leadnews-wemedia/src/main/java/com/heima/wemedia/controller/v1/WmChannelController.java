package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.controller.v1
 */
@RestController
@RequestMapping("/api/v1/channel")
public class WmChannelController {


    @Resource
    private WmChannelService channelService;

    @GetMapping("channels")
    public ResponseResult channels(){
        return channelService.listAll();
    }


}
