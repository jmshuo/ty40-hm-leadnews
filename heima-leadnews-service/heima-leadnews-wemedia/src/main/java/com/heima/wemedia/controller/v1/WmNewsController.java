package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.wemedia.dtos.WmNewsListDto;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.controller.v1
 */
@RestController
@RequestMapping("/api/v1/news")
public class WmNewsController {

    @Resource
    private WmNewsService newsService;

    /**
     * news文章列表查询
     * @return
     */
    @PostMapping("list")
    public PageResponseResult list(@Valid @RequestBody WmNewsListDto dto){
        return newsService.listForPage(dto);
    }


}
