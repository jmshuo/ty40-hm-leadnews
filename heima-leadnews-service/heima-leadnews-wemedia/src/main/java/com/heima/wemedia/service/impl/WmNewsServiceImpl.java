package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsListDto;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.service.WmChannelService;
import com.heima.wemedia.service.WmNewsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.service.impl
 */
@Slf4j
@Service
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {


    /**
     * 分页查询文章列表
     * @param dto
     * @return
     */
    @Override
    public PageResponseResult listForPage(WmNewsListDto dto) {

        LambdaQueryWrapper<WmNews> lambdaQueryWrapper = Wrappers.<WmNews>lambdaQuery();

        if (dto.getStatus()!=null){
            lambdaQueryWrapper.eq(WmNews::getStatus, dto.getStatus());
        }

        if (dto.getChannelId()!=null){
            lambdaQueryWrapper.eq(WmNews::getChannelId, dto.getChannelId());
        }

        if (dto.getBeginPubDate()!=null){
            lambdaQueryWrapper.ge(WmNews::getCreatedTime,dto.getBeginPubDate());
        }

        if (dto.getEndPubDate()!=null){
            lambdaQueryWrapper.le(WmNews::getCreatedTime,dto.getEndPubDate());
        }

        if (StringUtils.isNotBlank(dto.getKeyword())){
            lambdaQueryWrapper.like(WmNews::getTitle, dto.getKeyword());
        }

        Page page = this.page(new Page(dto.getPage(), dto.getSize()), lambdaQueryWrapper);

        PageResponseResult pageResult = new PageResponseResult(dto.getPage(),dto.getSize(),(int)page.getTotal());
        pageResult.setData(page.getRecords());
        return pageResult;
    }
}
