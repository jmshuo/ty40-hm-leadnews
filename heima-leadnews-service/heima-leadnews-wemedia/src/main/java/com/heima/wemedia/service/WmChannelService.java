package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialListDto;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.service
 */
public interface WmChannelService extends IService<WmChannel> {

    /**
     * 查询所有评到
     * @return
     */
    ResponseResult listAll();
}
