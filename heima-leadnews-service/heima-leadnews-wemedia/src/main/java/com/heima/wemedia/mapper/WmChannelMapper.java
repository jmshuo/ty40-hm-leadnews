package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.mapper
 */
@Mapper
public interface WmChannelMapper extends BaseMapper<WmChannel> {
}
