package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialListDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.spring.web.readers.operation.ResponseMessagesReader;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.service.impl
 */
@Slf4j
@Service
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {

    @Resource
    private FileStorageService fileStorageService;

    /**
     * 上传图片素材
     *
     * @param multipartFile
     * @return
     */
    @Override
    public ResponseResult uploadPicture(MultipartFile multipartFile) {


        /**
         * 上传图片素材到MINIO
         */
        // 1. 文件名
        String fileName = UUID.randomUUID().toString().replace("-", "");
        String originalFilename = multipartFile.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fullFileName = fileName + suffix;

        String path = null;
        // 2. 上传文件
        try {
            path = fileStorageService.uploadImgFile("", fullFileName, multipartFile.getInputStream());
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        /**
         * 保存素材信息到DB
         */
        WmMaterial material = new WmMaterial();
        material.setUrl(path);
        material.setUserId(WmThreadLocalUtil.getUser().getId());
        material.setIsCollection((short)0);
        material.setType((short)0);
        material.setCreatedTime(new Date());

        this.save(material);

        return ResponseResult.okResult(material);
    }

    /**
     * 分页查询素材列表
     * @param dto
     * @return
     */
    @Override
    public PageResponseResult listForPage(WmMaterialListDto dto) {
        IPage page = new Page(dto.getPage(),dto.getSize());
        LambdaQueryWrapper<WmMaterial> lambdaQueryWrapper = Wrappers.<WmMaterial>lambdaQuery();
        if (dto.getIsCollection() != null && dto.getIsCollection() == 1){
            lambdaQueryWrapper.eq(WmMaterial::getIsCollection, 1);
        }
        // 查询
        page = this.page(page, lambdaQueryWrapper);
        PageResponseResult responseResult = new PageResponseResult(dto.getPage(),dto.getSize(),(int)page.getTotal());
        responseResult.setData(page.getRecords());
        return responseResult;
    }
}
