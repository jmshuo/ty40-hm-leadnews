package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialListDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.service
 */
public interface WmMaterialService extends IService<WmMaterial> {
    /**
     * 上传图片素材
     * @param multipartFile
     * @return
     */
    ResponseResult uploadPicture(MultipartFile multipartFile);

    /**
     * 分页查询素材列表
     * @param dto
     * @return
     */
    PageResponseResult listForPage(WmMaterialListDto dto);
}
