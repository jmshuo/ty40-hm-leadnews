package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialListDto;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.controller.v1
 */
@RestController
@RequestMapping("/api/v1/material")
public class WmMaterialController {

    @Resource
    private WmMaterialService materialService;

    /**
     * 上传素材
     * @param multipartFile
     * @return
     */
    @PostMapping("upload_picture")
    public ResponseResult uploadPicture(MultipartFile multipartFile){
        return materialService.uploadPicture(multipartFile);
    }

    /**
     * 素材列表展示
     * @return
     */
    @PostMapping("list")
    public PageResponseResult list(@Valid @RequestBody WmMaterialListDto dto){
        return materialService.listForPage(dto);
    }


}
