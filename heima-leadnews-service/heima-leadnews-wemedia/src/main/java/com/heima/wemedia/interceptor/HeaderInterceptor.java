package com.heima.wemedia.interceptor;

import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.thread.WmThreadLocalUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.wemedia.interceptor
 */
public class HeaderInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String id = request.getHeader("id");
        // 判断header中是否获取到了ID
        if (StringUtils.isNotBlank(id)) {
            WmUser user = new WmUser();
            user.setId(Integer.valueOf(id));
            // 将用户对象放入ThreadLocal
            WmThreadLocalUtil.setUser(user);
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        WmThreadLocalUtil.clear();
    }
}
