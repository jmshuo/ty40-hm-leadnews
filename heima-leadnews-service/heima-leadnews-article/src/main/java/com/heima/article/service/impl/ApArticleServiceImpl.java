package com.heima.article.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.constants.ArticleConstants;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
@Slf4j
public class ApArticleServiceImpl  extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {

    // 单页最大加载的数字
    private final static short MAX_PAGE_SIZE = 50;

    @Resource
    private ApArticleMapper apArticleMapper;


    /**
     * 默认加载05
     * @param dto
     * @return
     */
    @Override
    public ResponseResult load(ArticleHomeDto dto) {
        Short type = ArticleConstants.LOAD_MORE_TYPE;
        return load(dto,type);
    }

    /**
     * 记载
     * @param dto 参数
     * @param type 加载类型
     * @return
     */
    @Override
    public ResponseResult load(ArticleHomeDto dto, short type) {
        List<ApArticle> apArticles = apArticleMapper.loadArticleList(dto, type);
        return ResponseResult.okResult(apArticles);
    }
}