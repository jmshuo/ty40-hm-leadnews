package com.heima.article.constants;

/**
 * @author guozidi  2023/5/7
 * @Description
 * @package com.heima.article.constants
 */
public interface ArticleConstants {

    short LOAD_MORE_TYPE= 1;
    short LOAD_TYPE = 1;
    short LOAD_NEW_TYPE = 2;
}
