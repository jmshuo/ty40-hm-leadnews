package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

import java.io.IOException;

public interface ApArticleService extends IService<ApArticle> {

    /**
     * 加载
     * @param dto
     * @return
     */
    ResponseResult load(ArticleHomeDto dto);

    /**
     * 加载
     * @param dto
     * @param type
     * @return
     */
    ResponseResult load(ArticleHomeDto dto, short type);
}