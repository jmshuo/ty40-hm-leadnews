package com.heima.utils.thread;

import com.heima.model.wemedia.pojos.WmUser;

/**
 * @author guozidi  2023/5/9
 * @Description
 * @package com.heima.utils.thread
 */
public class WmThreadLocalUtil {

    private static final ThreadLocal<WmUser> WM_USER_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 将用户存入线程
     * @param user
     */
    public static void setUser(WmUser user){
        WM_USER_THREAD_LOCAL.set(user);
    }

    /**
     * 从线程中获取用户
     * @return
     */
    public static WmUser getUser(){
        return WM_USER_THREAD_LOCAL.get();
    }

    /**
     * 清理
     */
    public static void clear(){
        WM_USER_THREAD_LOCAL.remove();
    }

}
