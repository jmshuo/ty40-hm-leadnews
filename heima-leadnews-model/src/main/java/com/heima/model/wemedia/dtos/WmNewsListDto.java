package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

import java.util.Date;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.model.wemedia.dtos
 */
@Data
public class WmNewsListDto extends PageRequestDto {


    private Short status;
    private Date beginPubDate;
    private Date endPubDate;
    private Integer channelId;
    private String keyword;


}
