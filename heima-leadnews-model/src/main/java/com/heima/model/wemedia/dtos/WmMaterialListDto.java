package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author guozidi  2023/5/10
 * @Description
 * @package com.heima.model.wemedia.dtos
 */
@Data
public class WmMaterialListDto extends PageRequestDto {

    /**
     * 是否收藏
     */
    private Short isCollection;

}
