package com.heima.model.user.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author guozidi  2023/5/6
 * @Description
 * @package com.heima.model.user.dtos
 */
@ApiModel("登陆参数")
@Data
public class LoginDto {

    @ApiModelProperty(value="电话")
    private String phone;
    @ApiModelProperty(value="密码")
    private String password;


}
