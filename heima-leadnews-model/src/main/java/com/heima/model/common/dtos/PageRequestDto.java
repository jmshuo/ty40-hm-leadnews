package com.heima.model.common.dtos;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@Slf4j
public class PageRequestDto {

    @Max(value = 100, message = "每页查询条目数过多")
    @Min(value = 1, message = "每页查询条目数不可为0")
    protected Integer size = 10;

    @Min(value = 1, message = "每页查询条目数不可为0")
    protected Integer page = 1;

}
