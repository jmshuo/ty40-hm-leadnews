package com.heima.model.article.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleHomeDto {

    // 最大时间
    Date maxBehotTime = new Date(0);
    // 最小时间
    Date minBehotTime = new Date(20000000000000L);
    // 分页size
    Integer size = 10;
    // 频道ID
    String tag = "__all__";
}