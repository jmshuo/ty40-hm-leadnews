package com.heima.app.gateway.filter;

import com.heima.app.gateway.util.AppJwtUtil;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author guozidi  2023/5/7
 * @Description
 * @package com.heima.app.gateway.filter
 */
@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {


        // 1.获取请求、应答对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        // 2.对路径进行判断，看他是否是登陆
        if (request.getURI().getPath().contains("/login")) {
            // 2.1 如果是登陆，放行
            return chain.filter(exchange);
        }

        // 3.获取token
        String token = request.getHeaders().getFirst("token");
        // 4.如果token不存在，拒绝
        if (StringUtils.isBlank(token)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.setComplete();
        }

        try{
            // 4.1 token存在，校验 协议.数据.签名
            Claims claimsBody = AppJwtUtil.getClaimsBody(token);
            int result = AppJwtUtil.verifyToken(claimsBody);
            if (result>0) {
                // 4.1.1 校验失败，拒绝
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                response.setComplete();
            }
        } catch (Exception ex){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.setComplete();
        }

        // 5. 校验通过，放行
        return chain.filter(exchange);
    }


    @Override
    public int getOrder() {
        return 0;
    }
}
